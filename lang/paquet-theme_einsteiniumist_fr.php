<?php
if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
    // T
    'theme_wuwei_description' => 'Un thème gris. Largeur fixe. Menu horizontal. Thème Z adapté du thème Wordpress Einsteiniumist.',
    'theme_wuwei_slogan' => 'Thème Z adapté du thème Wordpress Einsteiniumist',
    'theme_wuwei_titre' => 'Einsteiniumist',
);